package com.wsi.destinationfinder.service;
import com.wsi.destinationfinder.exception.InputException;
import com.wsi.destinationfinder.impl.TransformAndValidateInput;
import com.wsi.destinationfinder.model.Input;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashSet;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

@Service
public class TransformAndValidateInputImpl implements TransformAndValidateInput {

    //to extract everything that is within [zipcode, weight]
    Pattern pattern = Pattern.compile("\\[(\\d{5}),(\\d+(\\.\\d+)?)\\]");
    //use to separate an array of string input
    Pattern pattern2 = Pattern.compile("\\[(.*?)\\]");


    public Collection<Input> parseInput(String[] arg) {

        Collection<Input> input = new HashSet<>();
        if (arg == null || arg.length == 0 ){
            throw new InputException("Inputs is empty");
        } else{
            StringBuilder stringBuilder = new StringBuilder();

            IntStream.range(0, arg.length).forEach(y -> {
                stringBuilder.append(arg[y]);
            });

            //JAVA9 new features: using match results to get a stream and use it to get a list/array of matches
            String[] matches = pattern2.matcher(stringBuilder.toString()).results().map(MatchResult::group).toArray(String[]::new);

            IntStream.range(0, matches.length).forEach(x -> {
                Matcher matcher = pattern.matcher(matches[x]);

                //map match result to Input object, if fail throw custom exception
                if (matcher.find()){
                    final String zipCode = matcher.group(1);
                    final double weight = Double.parseDouble(matcher.group(2));
                    input.add(new Input(zipCode, weight));
                } else {
                    throw new InputException("Invalid Input Element " + matches[x]);

                }
            });
        }
        return input;
    }
}