package com.wsi.destinationfinder.service;

import com.wsi.destinationfinder.impl.ComputeResult;
import com.wsi.destinationfinder.model.Input;
import com.wsi.destinationfinder.model.Output;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;


//Oversize Item definition > 100KG
@Service
public class ComputeResultImpl implements ComputeResult {

    @Autowired
    ZipCodeStateMatcher zipCodeStateMatcher;

    //compute the result base on the weight and the zip code. If the shipment is from the 48 states and is overweight, you can only ship it to the 48 states;
    //if the shipment is from outside of 48 states and is overweight, you can only ship it locally, otherwise the shipment can go across all states in US
    public Output computeResult(Collection<Input> inputs){
        Collection<Input> shipmentToAllStates = new ArrayList<>();
        Collection<Input> shipmentTo48States = new ArrayList<>() ;
        Collection<Input> localShipmentOnly = new ArrayList<>();
        Collection<String> errorShipCode = new ArrayList<>();

        inputs.forEach(x -> {

                String state;
                try {
                    //leverage an online api to get the state name and verified if it is a valid state
                    state = zipCodeStateMatcher.zipCodeMatcher(x.getZipCode()).getState();
                    if (x.getWeight() > 100.0) {
                        if (state.equalsIgnoreCase("AK") || state.equalsIgnoreCase("HI") || state.equalsIgnoreCase("PR")) {
                            localShipmentOnly.add(x);
                        } else {
                            shipmentTo48States.add(x);
                        }
                    }
                    else {
                        shipmentToAllStates.add(x);
                    }
                } catch(Exception e){
                        errorShipCode.add(x.getZipCode());
                }


        });
        return new Output(shipmentToAllStates, localShipmentOnly, shipmentTo48States, errorShipCode);
    }
}
