package com.wsi.destinationfinder.service;

import com.wsi.destinationfinder.exception.ZipCodeException;
import com.wsi.destinationfinder.model.APIResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class ZipCodeStateMatcher {
    public APIResponse zipCodeMatcher(String zipCode) {
        RestTemplate restTemplate = new RestTemplate();

        //suppose to hide the api key using system property, but just for demostration purpose here
        String apiKey = "j3lnuAnYA5V9LhTBiLPKyejwZ8FdyCsvd8o7SCqdA8yy6YrRP7bTSZZShBJNTG1N";
        String resourceURL = "https://www.zipcodeapi.com/rest/"+ apiKey +"/info.json/"+ zipCode +"/degrees";

        ResponseEntity<APIResponse> responseEntity;

        //throwing customize error when the api returns error
        try {
            responseEntity = restTemplate.getForEntity(resourceURL, APIResponse.class);
        } catch (HttpClientErrorException ex) {
            throw new ZipCodeException("Zip Code Not Found");
        }
        return responseEntity.getBody();
    }
}
