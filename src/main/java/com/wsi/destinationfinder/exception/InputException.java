package com.wsi.destinationfinder.exception;

public class InputException extends RuntimeException {
    public InputException(){
        super();
    }

    public InputException(String message){
        super(message);
    }
}
