package com.wsi.destinationfinder.exception;

public class ZipCodeException extends RuntimeException {
    public ZipCodeException() {super();}
    public ZipCodeException(String message){
        super(message);
    }
}
