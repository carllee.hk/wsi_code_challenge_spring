package com.wsi.destinationfinder.impl;

import com.wsi.destinationfinder.model.Input;
import com.wsi.destinationfinder.model.Output;

import java.util.Collection;

public interface ComputeResult {
    Output computeResult(Collection<Input> inputs);
}
