package com.wsi.destinationfinder.impl;

import com.wsi.destinationfinder.model.Input;

import java.util.Collection;

public interface TransformAndValidateInput {
    Collection<Input> parseInput(String[] arg);
}
