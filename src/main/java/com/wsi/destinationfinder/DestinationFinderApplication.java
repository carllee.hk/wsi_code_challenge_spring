package com.wsi.destinationfinder;

import com.wsi.destinationfinder.service.ComputeResultImpl;
import com.wsi.destinationfinder.model.Input;
import com.wsi.destinationfinder.model.Output;
import com.wsi.destinationfinder.impl.TransformAndValidateInput;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Collection;

import static java.lang.System.exit;

@SpringBootApplication

//A spring boot application for user to input an array of string in the format of: ([zicode, weight],[zipcode, weight],...).
//Base on the zip code and the weight of the shipment it will then calculate the respective output (which shipment can go to where) in the log
public class DestinationFinderApplication implements CommandLineRunner {

    @Autowired
    private TransformAndValidateInput transformAndValidateInput;

    @Autowired
    private ComputeResultImpl computeResult;

    private static Logger log = LoggerFactory.getLogger(DestinationFinderApplication.class);
    public static void main(String[] args) {

        SpringApplication.run(DestinationFinderApplication.class, args);

    }

    @Override
    public void run(String... args) throws Exception {

        Collection<Input> input = transformAndValidateInput.parseInput(args);
        Output outputs = computeResult.computeResult(input);
        log.info("The following shipments can be ship to all states in US: {}", outputs.getShipmentToAllStates());
        log.info("The following shipments can only be shipped locally: {}", outputs.getLocalShipmentOnly());
        log.info("The following shipments can be ship to all 48 contiguous US states (excluding AK, HI, PR): {}", outputs.getShipmentTo48States());
        log.error("These zip code is invalid: {}", outputs.getErrorZipCode());

        exit(0);

    }
}
