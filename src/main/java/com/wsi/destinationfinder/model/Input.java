package com.wsi.destinationfinder.model;

import java.util.Objects;

//POJO for input object with states zipCode and weight
public class Input {
    private String zipCode;
    private double weight;

    public Input(String zipCode, double weight) {
        this.zipCode = zipCode;
        this.weight = weight;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Input{" +
                "zipCode=" + zipCode +
                ", weight=" + weight +
                '}';
    }

    //override equals method for test case
    @Override
    public boolean equals(Object obj) {
        if (obj == this){
            return true;
        }

        if (!(obj instanceof Input)){
            return false;
        }

        Input input = (Input) obj;

        return Double.compare(weight, input.weight) == 0 && zipCode.equals(input.zipCode);

    }

    //override hashcode for test case
    @Override
    public int hashCode() {
        return Objects.hash(zipCode,weight);
    }
}
