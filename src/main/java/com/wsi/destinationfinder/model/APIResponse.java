package com.wsi.destinationfinder.model;

public class APIResponse {
    private String zip_code;
    private String lat;
    private String lng;
    private String city;
    private String state;

    public String getZip_Code() {
        return zip_code;
    }

    public void setZip_Code(String zip_Code) {
        this.zip_code = zip_Code;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("APIResponse{");
        sb.append("zip_Code='").append(zip_code).append('\'');
        sb.append(", lat='").append(lat).append('\'');
        sb.append(", lng='").append(lng).append('\'');
        sb.append(", city='").append(city).append('\'');
        sb.append(", state='").append(state).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
