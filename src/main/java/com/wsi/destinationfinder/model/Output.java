package com.wsi.destinationfinder.model;

import java.util.Collection;

//POJO for Output containing collection of shipmentToAllStates, localShipmentOnly, shipmentTo48States and errorZipCode
public class Output {
    private Collection<Input> shipmentToAllStates;
    private Collection<Input> localShipmentOnly;
    private Collection<Input> shipmentTo48States;
    private Collection<String> errorZipCode;

    public Output(Collection<Input> shipmentToAllStates, Collection<Input> localShipmentOnly, Collection<Input> shipmentTo48States, Collection<String> errorZipCode){
        this.shipmentTo48States = shipmentTo48States;
        this.localShipmentOnly = localShipmentOnly;
        this.shipmentToAllStates = shipmentToAllStates;
        this.errorZipCode = errorZipCode;
    }

    public Collection<Input> getShipmentToAllStates() {
        return shipmentToAllStates;
    }

    public void setShipmentToAllStates(Collection<Input> shipmentToAllStates) {
        this.shipmentToAllStates = shipmentToAllStates;
    }

    public Collection<Input> getLocalShipmentOnly() {
        return localShipmentOnly;
    }

    public void setLocalShipmentOnly(Collection<Input> localShipmentOnly) {
        this.localShipmentOnly = localShipmentOnly;
    }

    public Collection<Input> getShipmentTo48States() {
        return shipmentTo48States;
    }

    public void setShipmentTo48States(Collection<Input> shipmentTo48States) {
        this.shipmentTo48States = shipmentTo48States;
    }

    public Collection<String> getErrorZipCode() {
        return errorZipCode;
    }

    public void setErrorZipCode(Collection<String> errorZipCode) {
        this.errorZipCode = errorZipCode;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Output{");
        sb.append("shipmentToAllStates=").append(shipmentToAllStates);
        sb.append(", localShipmentOnly=").append(localShipmentOnly);
        sb.append(", shipmentTo48States=").append(shipmentTo48States);
        sb.append(", errorZipCode=").append(errorZipCode);
        sb.append('}');
        return sb.toString();
    }

}
