package com.wsi.destinationfinder;

import com.wsi.destinationfinder.exception.InputException;
import com.wsi.destinationfinder.service.TransformAndValidateInputImpl;
import com.wsi.destinationfinder.model.Input;
import com.wsi.destinationfinder.impl.TransformAndValidateInput;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Collection;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsCollectionContaining.hasItem;

public class TransformAndValidateInputTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    private TransformAndValidateInput transformAndValidateInput;

    @Before
    public void before(){
        transformAndValidateInput = new TransformAndValidateInputImpl();
    }

    @Test
    public void testParseInputWithoutArgument(){
        expectedException.expect(InputException.class);
        expectedException.expectMessage(containsString("Inputs is empty"));
        transformAndValidateInput.parseInput(null);
    }

    @Test
    public void testParseInputWithEmptyArg(){
        expectedException.expect(InputException.class);
        expectedException.expectMessage(containsString("Inputs is empty"));
        transformAndValidateInput.parseInput(new String[] {});
    }

    @Test
    public void testParseInputWithMalFormZipCode1(){
        //malform input 1 as zip code is more then 5 digits
        Input input1 = new Input("123456", 2);
        Input input2 = new Input("23456", 2);

        String[] args = new String[]{"([" + input1.getZipCode() + "," + input1.getWeight() +
                "]", "[" + input2.getZipCode() + "," + input2.getWeight() + "])"};
        expectedException.expect(InputException.class);
        expectedException.expectMessage(containsString("Invalid Input Element"));
        transformAndValidateInput.parseInput(args);
    }

    @Test
    public void testParseInputWithMalFormZipCode2(){
        //malform input 1 as zip code is less then 5 digits
        Input input1 = new Input("987", 2);
        Input input2 = new Input("23456", 2);

        String[] args = new String[]{"([" + input1.getZipCode() + "," + input1.getWeight() +
                "]", "[" + input2.getZipCode() + "," + input2.getWeight() + "])"};
        expectedException.expect(InputException.class);
        expectedException.expectMessage(containsString("Invalid Input Element"));
        transformAndValidateInput.parseInput(args);
    }

    @Test
    public void testParseInputWithMalFormZipCode3(){
        //malform input 2 as zip code contains alphabet
        Input input1 = new Input("98789", 2);
        Input input2 = new Input("234BC", 2);

        String[] args = new String[]{"([" + input1.getZipCode() + "," + input1.getWeight() +
                "]", "[" + input2.getZipCode() + "," + input2.getWeight() + "])"};
        expectedException.expect(InputException.class);
        expectedException.expectMessage(containsString("Invalid Input Element"));
        transformAndValidateInput.parseInput(args);
    }

    @Test
    public void testParseInputWithMalFormZipCode4(){
        //malform input 2 as zip code are all alphabet
        Input input1 = new Input("98789", 2);
        Input input2 = new Input("ABCDE", 2);

        String[] args = new String[]{"([" + input1.getZipCode() + "," + input1.getWeight() +
                "]", "[" + input2.getZipCode() + "," + input2.getWeight() + "])"};
        expectedException.expect(InputException.class);
        expectedException.expectMessage(containsString("Invalid Input Element"));
        transformAndValidateInput.parseInput(args);
    }

    @Test
    public void testParseInputWithMalFormZipCode5(){
        //malform input 1 as zip code is negative
        Input input1 = new Input("-98789", 2);
        Input input2 = new Input("23456", 2);

        String[] args = new String[]{"([" + input1.getZipCode() + "," + input1.getWeight() +
                "]", "[" + input2.getZipCode() + "," + input2.getWeight() + "])"};
        expectedException.expect(InputException.class);
        expectedException.expectMessage(containsString("Invalid Input Element"));
        transformAndValidateInput.parseInput(args);
    }

    @Test
    public void testParseInputWithMalFormWeight1(){
        //malform input 1 as weight is negative
        Input input1 = new Input("98789", -2.0);
        Input input2 = new Input("23456", 2.1);

        String[] args = new String[]{"([" + input1.getZipCode() + "," + input1.getWeight() +
                "]", "[" + input2.getZipCode() + "," + input2.getWeight() + "])"};
        expectedException.expect(InputException.class);
        expectedException.expectMessage(containsString("Invalid Input Element"));
        transformAndValidateInput.parseInput(args);
    }

    @Test
    public void testParseInputWithMalFormWeight2(){
        //malform input as weight contains alphabet
        String[] args = new String[]{"([11112,2.0]", "[11114,2.A])"};
        expectedException.expect(InputException.class);
        expectedException.expectMessage(containsString("Invalid Input Element"));
        transformAndValidateInput.parseInput(args);
    }

    @Test
    public void testParseInputWithMalFormWeight3(){
        //malform input as weight is all alphabet
        String[] args = new String[]{"([11112,weight]", "[11114,2.3])"};
        expectedException.expect(InputException.class);
        expectedException.expectMessage(containsString("Invalid Input Element"));
        transformAndValidateInput.parseInput(args);
    }

    @Test
    public void testParseInputWithMalFormInput(){
        //malform input as tuples only have 1 value
        String[] args = new String[]{"([11112]", "[11114,11115])"};
        expectedException.expect(InputException.class);
        expectedException.expectMessage(containsString("Invalid Input Element"));
        transformAndValidateInput.parseInput(args);
    }

    @Test
    public  void testParseInputWithValidInput(){
        Input input1 = new Input("12345", 2);
        Input input2 = new Input("23456", 3.0);
        String[] args = new String[]{"([" + input1.getZipCode() + "," + input1.getWeight() +
                "]", "[" + input2.getZipCode() + "," + input2.getWeight() + "])"};
        Collection<Input> collection = transformAndValidateInput.parseInput(args);

        assertThat(collection, hasItem(input1));
        assertThat(collection, hasItem(input2));
    }
}
