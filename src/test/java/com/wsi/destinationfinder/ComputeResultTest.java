package com.wsi.destinationfinder;

import com.wsi.destinationfinder.exception.ZipCodeException;
import com.wsi.destinationfinder.model.APIResponse;
import com.wsi.destinationfinder.model.Input;
import com.wsi.destinationfinder.model.Output;
import com.wsi.destinationfinder.service.ComputeResultImpl;
import com.wsi.destinationfinder.service.ZipCodeStateMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collection;
import java.util.HashSet;

import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.when;


public class ComputeResultTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    ComputeResultImpl computeResult;

    @Mock
    ZipCodeStateMatcher zipCodeStateMatcher;

    @Before
    public void init(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void computeResultTestWithAllResult() {
        //When weight is less then 100, and outside of 48 states
        Input input1 = new Input("96759", 12.0);
        //When weight is less then 100, and inside of 48 states
        Input input2 = new Input("10001", 10.3);
        //When weight is more then 100, and inside of 48 states
        Input input3 = new Input("10001", 102.8);
        //When weight is more then 100, and outside of 48 states
        Input input4 = new Input("96759", 200.3);
        //Invalid zip code, weight > 100
        Input input5 = new Input("12344", 1003);
        //Invalid zip code, weight < 100
        Input input6 = new Input("12344", 10.3);



        Collection<Input> inputs = new HashSet<>();
        inputs.add(input1);
        inputs.add(input2);
        inputs.add(input3);
        inputs.add(input4);
        inputs.add(input5);
        inputs.add(input6);

        APIResponse apiResponse = new APIResponse();
        APIResponse apiResponse2 = new APIResponse();

        //mock object for the api response from zip code matcher
        apiResponse.setState("NY");
        when(zipCodeStateMatcher.zipCodeMatcher("10001")).thenReturn(apiResponse);
        apiResponse2.setState("HI");
        when(zipCodeStateMatcher.zipCodeMatcher("96759")).thenReturn(apiResponse2);
        when(zipCodeStateMatcher.zipCodeMatcher("12344")).thenThrow(new ZipCodeException());

        //methods called to calculate the result
        Output output = computeResult.computeResult(inputs);



        assertThat(output.getShipmentToAllStates(), hasItems(input1, input2));
        assertThat(output.getShipmentTo48States(), hasItem(input3));
        assertThat(output.getLocalShipmentOnly(), hasItem(input4));
        assertThat(output.getErrorZipCode(),hasItems(input5.getZipCode(), input6.getZipCode()));

    }
}
