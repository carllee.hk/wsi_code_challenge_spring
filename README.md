# Destination Finder
A spring boot application where the user can input a string array in the following format: ([zicode, weight],[zipcode, weight],...). Based on the zip code and the weight of the shipment it will then calculate the respective output

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software

```
java 9
apache maven 3.x 

```

### Build the application

Run following command to build the application

```
mvn clean install
```

## Running the tests

Following command to run any test
```
java -jar target/wsi_code_challenge_spring.jar ([10032,2.0],[10345,1001.3])
java -jar target/wsi_code_challenge_spring.jar ([43212,1002.0],[10543,10.1])
```


  
  

## Requirements

 - JAVA 9
 - Maven

## License

Copyright (c) 2019 Carl Lee

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
